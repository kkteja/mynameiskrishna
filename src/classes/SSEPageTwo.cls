public class SSEPageTwo {

    //Variables
    //These are used to trasfer the data from front end to back end and viceversa.
    public string s{set;get;}    //'These are nothing but serirs of chars in single quotes'
    public integer i{set;get;}   // 1 or 2 or 3 so on and so forth
    public decimal d{set;get;}   // 1.00 or 2.00 or 3.00 so on and so forth 
    public boolean b{set;get;}   // true or false
    
    
    public List<Account> a{set;get;}
    
    //Constructors
    //This gets invoked when the class loads automatically, unlike methods
    public SSEPageTwo(){
        a = [select id,name,accountnumber from account];
    }
    
    
    
    //Methods
    //This gets invoked when the user clicks on the link or buttons
    public void methodOne(){
        system.debug('--------MethodOne-------');
    }
    
    
    
    

}