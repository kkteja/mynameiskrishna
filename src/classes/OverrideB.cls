public class OverrideB {


    public B__c b{set;get;}

    public OverrideB(ApexPages.StandardController controller){
        b = [select id, Last_Name__c, Name, account__r.name from B__c where id=: controller.getId()];
    
    }
    
    
    public void editCustom(){
    }

    public void deleteCustom(){
    }
    
    public void cloneCustom(){
    }
}