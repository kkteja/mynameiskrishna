public class VFSendSMSController {

    public String smsContent{set;get;}
    public PageReference sendSMS(){
    
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint('https://control.msg91.com/api/sendhttp.php?authkey=86635AKFCjfqcKYi5582f99f&sender=Adathn&mobiles=9885048068,8985509397&message='+ smsContent);
        req.setMethod('GET');
        
        Http http = new Http();
        HttpResponse res = http.send(req);    
        
        
        FeedItem fi = new FeedItem();
        fi.body = smsContent;
        fi.parentId = '00528000000I023';
        
        insert fi;
        
        FeedComment fc = new FeedComment();
        fc.commentBody = res.getBody();
        
        fc.feeditemid = fi.id;
        insert fc;
    
        return new PageReference('/'+fi.id);
    }
}