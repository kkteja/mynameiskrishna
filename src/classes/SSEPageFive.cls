public class SSEPageFive {

    public String v2 { get; set; }

    public String v1 { get; set; }

    public PageReference Save() {
    
        Account a = new Account();
        
        a.name = v1;
        
        a.accountnumber = v2;
        
        
        insert a;
    
    
        system.debug('-------------'+a);
    
        PageReference p =  new PageReference('/'+a.id);
    
        return p;
    }

}