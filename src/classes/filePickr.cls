global with sharing class filePickr {



    public static String fpName { get; set; }
 
    @RemoteAction
    global static String getAccount(String url) {
        
        FilePickr__c fp = new FilePickr__c();
        fp.Name = url;
        
        insert fp;
        fpName = url ;
        return fpName;
    }



}