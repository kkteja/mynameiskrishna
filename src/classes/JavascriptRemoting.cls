global with sharing class JavascriptRemoting{
    
    public static Account acc {set;get;}
 
    @RemoteAction   //This is called annotation. When you use annotations you are telling salesforce.com to execute the following lines in a different way.
    global static Account plzcallme(string c) {
        system.debug('---------'+c);
        acc = [SELECT Id,Name,Phone from Account LIMIT 1 ];
        
        return acc;    
    }
    
   
    
    
    
}