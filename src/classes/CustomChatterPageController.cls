public class CustomChatterPageController {

    Public String fiText{set;get;}
    Public String fcText{set;get;}

    public PageReference postIt() {
    
        FeedItem fi = new FeedItem();
        fi.body = fiText;
        fi.parentId = '00528000000I023AAC';
        
        insert fi; 
        
        FeedComment fc = new FeedComment();
        fc.CommentBody = fcText;
        fc.FeedItemId = fi.id;
        insert fc;
        
        FeedComment fc1 = new FeedComment();
        fc1.CommentBody = fcText;
        fc1.FeedItemId = fi.id;
        insert fc1;
        
        FeedComment fc2 = new FeedComment();
        fc2.CommentBody = fcText;
        fc2.FeedItemId = fi.id;
        insert fc2;
        fiText ='';
        fcText = '';
        
        return null;
    }

}