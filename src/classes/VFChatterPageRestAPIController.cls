public class VFChatterPageRestAPIController{

    public String responseVar{set;get;}
    public void postIt(){
    
    
        HttpRequest req = new HttpRequest();
        
        req.setEndpoint('https://ap2.salesforce.com/services/data/v34.0/chatter/feed-elements');
        req.setMethod('POST');
        
        req.setHeader('Authorization','OAuth '+UserInfo.getSessionId());
        req.setHeader('Content-Type','application/json');

        req.setBody('{    "body" : {      "messageSegments" : [         {            "type" : "Text",            "text" : "We are trying to do it with REST API "         },         {               "type" : "Mention",            "id" : "00528000000ILoq"}        ]       },   "feedElementType" : "FeedItem",   "subjectId" : "00528000000I023" }');
        
        Http http = new Http();
        HttpResponse res = http.send(req);
        
        responseVar  = res.getBody();
        
    
    }

}