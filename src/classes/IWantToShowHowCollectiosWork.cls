public class IWantToShowHowCollectiosWork {

    
    
    //Declaring
    public List<Decimal> myList {set;get;}


    public IWantToShowHowCollectiosWork(){
        
        
        //Instantiation
        myList = new List<Decimal>();
        
        
        //Calling methods in List class
        myList.add(47.0);
        myList.add(48.1);
        myList.add(49.50);
        myList.add(47.60);
        
        
       
    
    }


}